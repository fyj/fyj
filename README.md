# fyj
> 🔖 **fyj** is a simple bookmarks manager written in Go language.

<img src="./assets/screenshots/home.png" width="600" alt="Screenshot">

## 功能
- 支持基本的增删减的操作, 以及搜索功能
- 支持 Netscape 书签文件的导入和导出
- 支持从 [Pocket](https://getpocket.com) 导入书签
- 运行通过命令行处理你的书签
- 如果你不善于使用命令行, 你也可以使用我们提供简约的 web 界面
- 具有便携性, 把前端打包进了二进制文件里
- 支持 SQLite, PostgreSQL 和 MySQL 数据库
- 可以自动提取网页的内容
- 可以把网页打包成归档文件, 所以即使是离线下依然保存你的数据
- 支持浏览器扩展(Chrome 和 Firefox) [BETA]

<img src="./assets/screenshots/read-mode.png" width="600" alt="Reader mode">

<img src="./assets/screenshots/archive-mode.png" width="600" alt="Archive mode">

## 开发
开发环境要求:

Windows: 安装 [git](https://git-scm.com/), make, [nodejs](https://nodejs.org/en/), [go](https://golang.google.cn)...
> 推荐安装 [scoop](https://github.com/lukesampson/scoop) 软件管理
```PowerShell
scoop install git make nodejs go
```
Linux: nodejs 安装建议使用 [nvm](https://github.com/nvm-sh/nvm)
```shell
$ sudo apt install git make -y
$ nvm install node
```
Mac: 参考 Linux

## 编译
```shell
$ npm i               # 安装 nodejs 依赖库
$ npm run build       # 或者 'npx gulp', 编译 less 文件和必要的 js 库
$ go generate         # 或者 'go run embed_assets.go', 把前端文件打包进 go 文件
$ make                # 编译二进制文件, 文件在 build 文件夹
```

## 协议
> `fyj` 遵守 [MIT](./LICENSE) 协议

你可以随意改动, 只要注明我 [@huiyifyj](https://github.com/huiyifyj) 即可
