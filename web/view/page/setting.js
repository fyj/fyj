var template = `
<div id="page-setting">
    <h1 class="page-header">Settings</h1>
    <div class="setting-container">
        <details open class="setting-group" id="setting-display">
            <summary>显示</summary>
            <label>
                <input type="checkbox" v-model="appOptions.showId" @change="saveSetting">
                显示书签的 ID
            </label>
            <label>
                <input type="checkbox" v-model="appOptions.listMode" @change="saveSetting">
                以列表的形式显示
            </label>
            <label>
                <input type="checkbox" v-model="appOptions.hideThumbnail" @change="saveSetting">
                隐藏缩略图
            </label>
            <label>
                <input type="checkbox" v-model="appOptions.hideExcerpt" @change="saveSetting">
                隐藏书签的简介文字
            </label>
            <label>
                <input type="checkbox" v-model="appOptions.nightMode" @change="saveSetting">
                开启黑夜模式
            </label>
        </details>
        <details v-if="activeAccount.owner" open class="setting-group" id="setting-bookmarks">
            <summary>书签</summary>
            <label>
                <input type="checkbox" v-model="appOptions.keepMetadata" @change="saveSetting">
                更新时依然保持书签元数据
            </label>
            <label>
                <input type="checkbox" v-model="appOptions.useArchive" @change="saveSetting">
                默认创建归档
            </label>
            <label>
                <input type="checkbox" v-model="appOptions.makePublic" @change="saveSetting">
                归档文件默认允许其他人可见
            </label>
        </details>
        <details v-if="activeAccount.owner" open class="setting-group" id="setting-accounts">
            <summary>账号</summary>
            <ul>
                <li v-if="accounts.length === 0">无任何注册账号</li>
                <li v-for="(account, idx) in accounts">
                    <p>{{account.username}}
                        <span v-if="account.owner" class="account-level">(owner)</span>
                    </p>
                    <a title="Change password" @click="showDialogChangePassword(account)">
                        <i class="fa fas fa-fw fa-key"></i>
                    </a>
                    <a title="Delete account" @click="showDialogDeleteAccount(account, idx)">
                        <i class="fa fas fa-fw fa-trash-alt"></i>
                    </a>
                </li>
            </ul>
            <div class="setting-group-footer">
                <a @click="loadAccounts">刷新账号</a>
                <a v-if="activeAccount.owner" @click="showDialogNewAccount">添加账号</a>
            </div>
        </details>
    </div>
    <div class="loading-overlay" v-if="loading"><i class="fas fa-fw fa-spin fa-spinner"></i></div>
    <custom-dialog v-bind="dialog"/>
</div>`;

import customDialog from "../component/dialog.js";
import basePage from "./base.js";

export default {
	template: template,
	mixins: [basePage],
	components: {
		customDialog
	},
	data() {
		return {
			loading: false,
			accounts: []
		}
	},
	methods: {
		saveSetting() {
			this.$emit("setting-changed", {
				showId: this.appOptions.showId,
				listMode: this.appOptions.listMode,
				hideThumbnail: this.appOptions.hideThumbnail,
				hideExcerpt: this.appOptions.hideExcerpt,
				nightMode: this.appOptions.nightMode,
				keepMetadata: this.appOptions.keepMetadata,
				useArchive: this.appOptions.useArchive,
				makePublic: this.appOptions.makePublic,
			});
		},
		loadAccounts() {
			if (this.loading) return;

			this.loading = true;
			fetch(new URL("api/accounts", document.baseURI))
				.then(response => {
					if (!response.ok) throw response;
					return response.json();
				})
				.then(json => {
					this.loading = false;
					this.accounts = json;
				})
				.catch(err => {
					this.loading = false;
					this.getErrorMessage(err).then(msg => {
						this.showErrorDialog(msg);
					})
				});
		},
		showDialogNewAccount() {
			this.showDialog({
				title: "新建账号",
				content: "输入新账号的信息:",
				fields: [{
					name: "username",
					label: "用户名",
					value: "",
				}, {
					name: "password",
					label: "密码",
					type: "password",
					value: "",
				}, {
					name: "repeat",
					label: "再次输入密码",
					type: "password",
					value: "",
				}, {
					name: "visitor",
					label: "只允许账号以访客形式访问",
					type: "check",
					value: false,
				}],
				mainText: "确定",
				secondText: "取消",
				mainClick: (data) => {
					if (data.username === "") {
						this.showErrorDialog("用户名不允许为空");
						return;
					}

					if (data.password === "") {
						this.showErrorDialog("密码不允许为空");
						return;
					}

					if (data.password !== data.repeat) {
						this.showErrorDialog("两次密码不匹配");
						return;
					}

					var request = {
						username: data.username,
						password: data.password,
						owner: !data.visitor,
					}

					this.dialog.loading = true;
					fetch(new URL("api/accounts", document.baseURI), {
						method: "post",
						body: JSON.stringify(request),
						headers: {
							"Content-Type": "application/json",
						}
					}).then(response => {
						if (!response.ok) throw response;
						return response;
					}).then(() => {
						this.dialog.loading = false;
						this.dialog.visible = false;

						this.accounts.push({ username: data.username, owner: !data.visitor });
						this.accounts.sort((a, b) => {
							var nameA = a.username.toLowerCase(),
								nameB = b.username.toLowerCase();

							if (nameA < nameB) {
								return -1;
							}

							if (nameA > nameB) {
								return 1;
							}

							return 0;
						});
					}).catch(err => {
						this.dialog.loading = false;
						this.getErrorMessage(err).then(msg => {
							this.showErrorDialog(msg);
						})
					});
				}
			});
		},
		showDialogChangePassword(account) {
			this.showDialog({
				title: "更改密码",
				content: "输入新的密码:",
				fields: [{
					name: "oldPassword",
					label: "旧密码",
					type: "password",
					value: "",
				}, {
					name: "password",
					label: "新密码",
					type: "password",
					value: "",
				}, {
					name: "repeat",
					label: "再次输入新密码",
					type: "password",
					value: "",
				}],
				mainText: "OK",
				secondText: "Cancel",
				mainClick: (data) => {
					if (data.oldPassword === "") {
						this.showErrorDialog("就密码不允许为空");
						return;
					}

					if (data.password === "") {
						this.showErrorDialog("新密码不允许为空");
						return;
					}

					if (data.password !== data.repeat) {
						this.showErrorDialog("两次新密码不匹配");
						return;
					}

					var request = {
						username: account.username,
						oldPassword: data.oldPassword,
						newPassword: data.password,
						owner: account.owner,
					}

					this.dialog.loading = true;
					fetch(new URL("api/accounts", document.baseURI), {
						method: "put",
						body: JSON.stringify(request),
						headers: {
							"Content-Type": "application/json",
						},
					}).then(response => {
						if (!response.ok) throw response;
						return response;
					}).then(() => {
						this.dialog.loading = false;
						this.dialog.visible = false;
					}).catch(err => {
						this.dialog.loading = false;
						this.getErrorMessage(err).then(msg => {
							this.showErrorDialog(msg);
						})
					});
				}
			});
		},
		showDialogDeleteAccount(account, idx) {
			this.showDialog({
				title: "删除账号",
				content: `确定要删除账号 "${account.username}" ?`,
				mainText: "确定",
				secondText: "取消",
				mainClick: () => {
					this.dialog.loading = true;
					fetch(`/api/accounts`, {
						method: "delete",
						body: JSON.stringify([account.username]),
						headers: {
							"Content-Type": "application/json",
						},
					}).then(response => {
						if (!response.ok) throw response;
						return response;
					}).then(() => {
						this.dialog.loading = false;
						this.dialog.visible = false;
						this.accounts.splice(idx, 1);
					}).catch(err => {
						this.dialog.loading = false;
						this.getErrorMessage(err).then(msg => {
							this.showErrorDialog(msg);
						})
					});
				}
			});
		},
	},
	mounted() {
		this.loadAccounts();
	}
}
