# Make sure you install `make` and `git` tool

# Get OS name: Linux, MINGW64_NT-10.0 or others
OS_NAME ?= $(shell uname -s)

ifeq ($(OS_NAME), Linux)
	BIN = fyj
else
	BIN = fyj.exe
endif

VERSION = $(shell git describe --tags `git rev-list --tags --max-count=1`)
COMMIT_ID = $(shell git rev-parse --short HEAD)
BUILD_DATE = $(shell date +'%Y-%m-%d')
BUILD_TIME = $(shell date +'%T')

all: build

build: clean
	@go build -o build/$(BIN) -ldflags \
	"-X gitea.com/huiyifyj/fyj/src/cmd.Version=${VERSION} \
	-X gitea.com/huiyifyj/fyj/src/cmd.commitID=${COMMIT_ID} \
	-X gitea.com/huiyifyj/fyj/src/cmd.buildDate=${BUILD_DATE} \
	-X gitea.com/huiyifyj/fyj/src/cmd.buildTime=${BUILD_TIME}"

clean:
	@if [ -f "build/$(BIN)" ]; then \
		rm -rf build/$(BIN); \
	fi;

test:
	@go test ./... -v

.PHONY: build clean test
