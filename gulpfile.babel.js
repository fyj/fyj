/**
 * This is a file that is used to run gulp tasks.
 * Copyright (c) 2020
 * Author: Huiyi.FYJ | i@huiyifyj.cn
 * License: [MIT](https://opensource.org/licenses/MIT)
 */
import { src, dest, series, parallel } from 'gulp'
import less from 'gulp-less'
import size from 'gulp-size'
import mincss from 'gulp-clean-css'
import rename from 'gulp-rename'
import uglify from 'gulp-uglify'
import gulpif from 'gulp-if'
import del from 'del'

/**
 * Delete files that you wish to delete.
 * @param {string | string[]} files This parameter follows **glob** match
 */
const clean = files => del(files)

const css = () => {
    clean('web/css/*')

    return src('web/less/*')
        .pipe(gulpif(/\.less$/, less()))
        .pipe(mincss())
        .pipe(size({
            showFiles: true,
            showTotal: false
        }))
        .pipe(dest('web/css'))
}

const font = () => {
    return src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
        .pipe(size({
            showFiles: true,
            showTotal: false
        }))
        .pipe(dest('web/css/fonts'))
}

const minjs = () => {
    clean('web/js/*')

    return src([
            'node_modules/dayjs/dayjs.min.js',
            'node_modules/domurl/url.js',
            'node_modules/vue/dist/vue.js'
        ])
        .pipe(gulpif(/^((?!min\.js).)+$/, uglify()))
        .pipe(gulpif(/^((?!min\.js).)+$/, rename({
            suffix: '.min'
        })))
        .pipe(size({
            showFiles: true,
            showTotal: false
        }))
        .pipe(dest('web/js'))
}

export const icon = () => {
    clean('web/res/*')

    return src([
            'assets/favicon.ico',
            'assets/icon/fyj-16.png',
            'assets/icon/fyj-32.png',
            'assets/icon/fyj-144.png',
            'assets/icon/fyj-152.png'
        ])
        .pipe(gulpif('fyj-16.png', rename({
            basename: 'favicon-16x16'
        })))
        .pipe(gulpif('fyj-32.png', rename({
            basename: 'favicon-32x32'
        })))
        .pipe(gulpif('fyj-144.png', rename({
            basename: 'apple-touch-icon-144x144'
        })))
        .pipe(gulpif('fyj-152.png', rename({
            basename: 'apple-touch-icon-152x152'
        })))
        .pipe(size({
            showFiles: true,
            showTotal: false
        }))
        .pipe(dest('web/res'))
}

export default parallel(minjs, icon, series(css, font))

//=================================================
// Browser Extension
//=================================================

const ext_css = () => {
    clean('ext/css/*')

    return src('ext/less/*')
        .pipe(gulpif(/\.less$/, less()))
        .pipe(mincss())
        .pipe(size({
            showFiles: true,
            showTotal: false
        }))
        .pipe(dest('ext/css'))
}

const ext_font = () => {
    return src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
        .pipe(size({
            showFiles: true,
            showTotal: false
        }))
        .pipe(dest('ext/css/fonts'))
}

const ext_js = () => {
    clean('ext/js/browser-polyfill.*')

    return src([
            'node_modules/webextension-polyfill/dist/browser-polyfill.min.js',
            'node_modules/webextension-polyfill/dist/browser-polyfill.min.js.map',
        ])
        .pipe(size({
            showFiles: true,
            showTotal: false
        }))
        .pipe(dest('ext/js'))
}

const ext_icon = () => {
    clean('ext/icons/*')

    return src([
            'assets/fyj.svg',
            'assets/icon/fyj-16.png',
            'assets/icon/fyj-32.png',
            'assets/icon/fyj-48.png',
            'assets/icon/fyj-64.png',
            'assets/icon/fyj-96.png',
            'assets/icon/fyj-128.png',
            'assets/icon/ext/*'
        ])
        .pipe(size({
            showFiles: true,
            showTotal: false
        }))
        .pipe(dest('ext/icons'))
}

export const ext = parallel(ext_js, ext_icon, series(ext_css, ext_font))
