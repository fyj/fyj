//go:generate go run embed_assets.go
//go:generate go run man_pages.go
//go:generate go run shell_completions.go

package main

import (
	// Database driver
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"

	// Add this to prevent it removed by 'go mod tidy'
	_ "github.com/shurcooL/vfsgen"

	"github.com/sirupsen/logrus"

	"gitea.com/huiyifyj/fyj/src/cmd"
)

func main() {
	err := cmd.FyjCmd().Execute()
	if err != nil {
		logrus.Fatalln(err)
	}
}
