#!/usr/bin/env bash

GIT_VERSION=$(git describe --tags `git rev-list --tags --max-count=1`)

VERSION=$(echo ${GIT_VERSION#*v})

rcedit ./build/fyj.exe \
        --set-icon ./assets/fyj.ico \
        --set-version-string CompanyName "xn-02f Lab" \
        --set-version-string ProductName "xn-02f Lab" \
        --set-version-string FileDescription "🔖 fyj is a simple bookmarks manager written in Go language." \
        --set-version-string OriginalFilename "fyj.exe" \
        --set-version-string InternalName "fyj" \
        --set-version-string LegalCopyright "(c) All fyj contributors. MIT LICENSE." \
        --set-file-version ${VERSION} \
        --set-product-version ${VERSION} \
        --application-manifest ./script/windows/manifest.xml
