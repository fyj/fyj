﻿[CustomMessages]
AddToPath=Add to PATH (requires shell restart)
DesktopIconComment=🔖 fyj is a simple bookmarks manager written in Go language.
RunAfter=Run %1 after installation
Other=Other:
