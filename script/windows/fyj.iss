﻿; This is script file that is used to bundle files into setup installer.
; Copyright (c) 2020
; Author: Huiyi.FYJ | i@huiyifyj.cn
; License: [MIT](https://opensource.org/licenses/MIT)

; ID used to uniquely identify this application.
; Generated by PowerShell:
;     [guid]::NewGuid().ToString()
; or run 'New-Guid' in PowerShell console.
#define APP_ID 'dbc835e6-86c3-4fdc-8985-f1b2d48244ae'
#define APP_NAME 'fyj'
#define VERSION '0.0.2'
#define PUBLISHER 'xn-02f Lab'
#define URL 'https://huiyifyj.cn'
#define EXE_NAME 'fyj.exe'
#define ICON '..\..\assets\fyj.ico'
#define LICENSE '..\..\LICENSE'
#define OUTPUT_DIR '..\..\build'
#define OUTPUT_NAME 'fyj-setup'
#define WIZARD_IMAGE_FILE '.\image\inno-big.bmp'
#define WIZARD_SMALL_IMAGE_FILE '.\image\inno-small.bmp'

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={#APP_ID}
AppName={#APP_NAME}
AppVersion={#VERSION}
AppVerName={#APP_NAME} {#VERSION}
AppPublisher={#PUBLISHER}
AppPublisherURL={#URL}
AppSupportURL={#URL}
AppUpdatesURL={#URL}
DefaultDirName=C:\{#APP_NAME}
LicenseFile={#LICENSE}
OutputDir={#OUTPUT_DIR}
OutputBaseFilename={#OUTPUT_NAME}
; better size is 164x314
WizardImageFile={#WIZARD_IMAGE_FILE}
; better size is 55x55
WizardSmallImageFile={#WIZARD_SMALL_IMAGE_FILE}
SetupIconFile={#ICON}
UninstallDisplayIcon={#ICON}
Uninstallable=yes
; Remove the following line to run in admin mode (install for all users).
; 'PrivilegesRequiredOverridesAllowed=dialog' can alter mode by dialog.
PrivilegesRequired=lowest
DisableProgramGroupPage=yes
DisableReadyPage=no
DisableDirPage=no
DirExistsWarning=yes
Compression=lzma2
SolidCompression=yes
WizardStyle=modern

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl,.\i18n\EnglishMessages.isl"
Name: "chinesesimplified"; MessagesFile: ".\i18n\ChineseSimplified.isl,.\i18n\ChineseMessages.isl"

[Files]
Source: "..\..\build\fyj.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\web\*"; DestDir: "{app}\web"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "..\..\LICENSE"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\README.md"; DestDir: "{app}"; Flags: ignoreversion isreadme

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:Other}"; Flags: unchecked
Name: "addtopath"; Description: "{cm:AddToPath}"; GroupDescription: "{cm:Other}"; Flags: unchecked

[Icons]
Name: "{autoprograms}\{#APP_NAME}"; Filename: "{app}\{#EXE_NAME}"
Name: "{autodesktop}\{#APP_NAME}"; Filename: "{app}\{#EXE_NAME}"; Comment: "{cm:DesktopIconComment}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#EXE_NAME}"; Parameters: "serve"; Description: "{cm:LaunchProgram}"; Flags: nowait postinstall skipifsilent
