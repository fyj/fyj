#!/usr/bin/env bash

echo "Here is an inevitably incomplete list of MUCH-APPRECIATED CONTRIBUTORS --
people who have submitted patches, reported bugs, added translations, helped
answer newbie questions, and generally made 'fyj' that much better:
" > ./build/AUTHORS

git log --format='%aN <%aE>' | sort -u >> ./build/AUTHORS
