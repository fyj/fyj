// +build ignore

package main

import (
	"log"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra/doc"

	"gitea.com/huiyifyj/fyj/src/cmd"
)

var pathSeparator = string(os.PathSeparator)

// Run `go run That will get you a man page /tmp/test.3
func main() {
	cmd := cmd.FyjCmd()

	header := &doc.GenManHeader{
		Title:   "MINE",
		Section: "1",
	}

	pwd, _ := os.Getwd()
	pwd += pathSeparator + "build" + pathSeparator + "man"

	// Create './build/man' folder
	err := os.MkdirAll(pwd, os.ModePerm)
	if err != nil {
		logrus.Fatalln(err)
		os.Exit(1)
	}

	// Generate man page files
	if err := doc.GenManTree(cmd, header, pwd); err != nil {
		log.Fatal(err)
	}
}
