// +build dev

package webserver

import (
	"net/http"
)

var assets = http.Dir("web")

func init() {
	developmentMode = true
}
