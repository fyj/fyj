package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"

	"gitea.com/huiyifyj/fyj/src/database"
)

func printCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "print [indices]",
		Short: "打印已保存的文章书签",
		Long: `按数据库的索引显示已保存的文章书签
Show the saved bookmarks by its database index.
接受以空格为分隔的索引列表(例如, 5 6 23 4 110 45)
连字符范围(例如100-200)或者混合使用(例如, 1-3 7 9)
如果没有参数, 默认将打印所有文章书签记录`,
		Aliases: []string{"list", "ls"},
		Run:     printHandler,
	}

	cmd.Flags().BoolP("json", "j", false, "以 JSON 格式数据输出")
	cmd.Flags().BoolP("latest", "l", false, "按最新日期(而不是ID)对文章书签进行排序")
	cmd.Flags().BoolP("index-only", "i", false, "仅打印文章书签的索引")
	cmd.Flags().StringP("search", "s", "", "以指定关键词搜索文章书签")
	cmd.Flags().StringSliceP("tags", "t", []string{}, "打印指定标签的文章书签")
	cmd.Flags().StringSliceP("exclude-tags", "e", []string{}, "打印不包含该书签的文章书签")

	return cmd
}

func printHandler(cmd *cobra.Command, args []string) {
	// Read flags
	tags, _ := cmd.Flags().GetStringSlice("tags")
	keyword, _ := cmd.Flags().GetString("search")
	useJSON, _ := cmd.Flags().GetBool("json")
	indexOnly, _ := cmd.Flags().GetBool("index-only")
	orderLatest, _ := cmd.Flags().GetBool("latest")
	excludedTags, _ := cmd.Flags().GetStringSlice("exclude-tags")

	// Convert args to ids
	ids, err := parseStrIndices(args)
	if err != nil {
		cError.Printf("Failed to parse args: %v\n", err)
		return
	}

	// Read bookmarks from database
	orderMethod := database.DefaultOrder
	if orderLatest {
		orderMethod = database.ByLastModified
	}

	searchOptions := database.GetBookmarksOptions{
		IDs:          ids,
		Tags:         tags,
		ExcludedTags: excludedTags,
		Keyword:      keyword,
		OrderMethod:  orderMethod,
	}

	bookmarks, err := db.GetBookmarks(searchOptions)
	if err != nil {
		cError.Printf("Failed to get bookmarks: %v\n", err)
		return
	}

	if len(bookmarks) == 0 {
		switch {
		case len(ids) > 0:
			cError.Println("No matching index found")
		case keyword != "", len(tags) > 0:
			cError.Println("No matching bookmarks found")
		default:
			cError.Println("No bookmarks saved yet")
		}
		return
	}

	// Print data
	if useJSON {
		bt, err := json.MarshalIndent(&bookmarks, "", "    ")
		if err != nil {
			cError.Println(err)
			return
		}

		fmt.Println(string(bt))
		return
	}

	if indexOnly {
		for _, bookmark := range bookmarks {
			fmt.Printf("%d ", bookmark.ID)
		}

		fmt.Println()
		return
	}

	printBookmarks(bookmarks...)
}
