package cmd

import (
	"fmt"
	"net/http"
	"sort"
	"sync"
	"time"

	"github.com/spf13/cobra"

	"gitea.com/huiyifyj/fyj/src/database"
	"gitea.com/huiyifyj/fyj/src/model"
)

func checkCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "check",
		Short: "查找互联网上不再存在的站点的文章书签",
		Long: `检查所有书签的源站点 URL 并找到那些互联网上已经不再存在的站点
可能需要很长时间, 具体取决于您要检查的书签数量以及你自身所处的网络状况
如果没有参数, 默认将检查所有书签`,
		Run: checkHandler,
	}

	cmd.Flags().BoolP("yes", "y", false, "跳过确认提示, 并且检查所有书签")

	return cmd
}

func checkHandler(cmd *cobra.Command, args []string) {
	// Parse flags
	skipConfirm, _ := cmd.Flags().GetBool("yes")

	// If no arguments (i.e all bookmarks going to be checked), confirm to user
	if len(args) == 0 && !skipConfirm {
		confirmCheck := ""
		fmt.Print("Check ALL bookmarks? (y/N): ")
		fmt.Scanln(&confirmCheck)

		if confirmCheck != "y" {
			fmt.Println("No bookmarks checked")
			return
		}
	}

	// Convert args to ids
	ids, err := parseStrIndices(args)
	if err != nil {
		cError.Printf("Failed to parse args: %v\n", err)
		return
	}

	// Fetch bookmarks from database
	filterOptions := database.GetBookmarksOptions{IDs: ids}
	bookmarks, err := db.GetBookmarks(filterOptions)
	if err != nil {
		cError.Printf("Failed to get bookmarks: %v\n", err)
		return
	}

	// Create HTTP client
	httpClient := &http.Client{Timeout: time.Minute}

	// Test each bookmark item
	unreachableIDs := []int{}

	wg := sync.WaitGroup{}
	chDone := make(chan struct{})
	chProblem := make(chan int, 10)
	chMessage := make(chan interface{}, 10)
	semaphore := make(chan struct{}, 10)

	for i, book := range bookmarks {
		wg.Add(1)

		go func(i int, book model.Bookmark) {
			// Make sure to finish the WG
			defer wg.Done()

			// Register goroutine to semaphore
			semaphore <- struct{}{}
			defer func() {
				<-semaphore
			}()

			// Ping bookmark's URL
			_, err := httpClient.Get(book.URL)
			if err != nil {
				chProblem <- book.ID
				chMessage <- fmt.Errorf("Failed to reach %s: %v", book.URL, err)
				return
			}

			// Send success message
			chMessage <- fmt.Sprintf("Reached %s", book.URL)
		}(i, book)
	}

	// Watch messages from channels
	go func(nBookmark int) {
		logIndex := 0

		for {
			select {
			case <-chDone:
				cInfo.Println("Check finished")
				return
			case id := <-chProblem:
				unreachableIDs = append(unreachableIDs, id)
			case msg := <-chMessage:
				logIndex++

				switch msg.(type) {
				case error:
					cError.Printf("[%d/%d] %v\n", logIndex, nBookmark, msg)
				case string:
					cInfo.Printf("[%d/%d] %s\n", logIndex, nBookmark, msg)
				}
			}
		}
	}(len(bookmarks))

	// Wait until all download finished
	wg.Wait()
	close(chDone)

	// Print the unreachable bookmarks
	fmt.Println()

	if len(unreachableIDs) == 0 {
		cInfo.Println("All bookmarks is reachable.")
	} else {
		sort.Ints(unreachableIDs)

		cError.Println("Encountered some unreachable bookmarks:")
		for _, id := range unreachableIDs {
			cError.Printf("%d ", id)
		}
		fmt.Println()
	}
}
