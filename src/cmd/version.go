package cmd

import (
	"fmt"
	"runtime"

	"github.com/spf13/cobra"
)

// Version information for cmd
// Use 'var' (not const) to defined variable for `go build -ldflags`
// And to expose to 'Version' variable
var (
	Version   = ""
	commitID  = ""
	buildDate = ""
	buildTime = ""
)

func versionCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "打印版本信息以及构建时的信息",
		Long: `打印版本信息, 当你遇见 bug 时可以提供相关信息去反馈
同时它也会打印具体的修订版信息和编译时间信息.`,
		Run: showVersion,
	}

	return cmd
}

// showVersion prints the version information to stdout
func showVersion(cmd *cobra.Command, args []string) {
	fmt.Printf("FYJ  %s\n", Version)
	fmt.Println("---------------------------------")
	fmt.Printf("- OS:         %s %s\n", runtime.GOOS, runtime.GOARCH)
	fmt.Printf("- Go version: %s\n", runtime.Version())
	fmt.Printf("- Revision:   %s\n", commitID)
	fmt.Printf("- Build time: %s %s\n", buildDate, buildTime)
}
