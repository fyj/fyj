package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"

	"gitea.com/huiyifyj/fyj/src/core"
	"gitea.com/huiyifyj/fyj/src/model"
)

func addCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "add [url]",
		Short: "添加指定 URL 作为文章书签",
		Args:  cobra.ExactArgs(1),
		Run:   addHandler,
	}

	cmd.Flags().StringP("title", "i", "", "自定义书签的标题")
	cmd.Flags().StringP("excerpt", "e", "", "自定义书签的内容简介")
	cmd.Flags().StringSliceP("tags", "t", []string{}, "自定义书签的标签, 如果是多个标签用逗号分隔")
	cmd.Flags().BoolP("offline", "o", false, "离线模式下保存书签(无需连接互联网)")
	cmd.Flags().BoolP("no-archival", "a", false, "不创建离线归档文件")
	cmd.Flags().Bool("log-archival", false, "处理归档文件时打印 log 日志信息")

	return cmd
}

func addHandler(cmd *cobra.Command, args []string) {
	// Read flag and arguments
	url := args[0]
	title, _ := cmd.Flags().GetString("title")
	excerpt, _ := cmd.Flags().GetString("excerpt")
	tags, _ := cmd.Flags().GetStringSlice("tags")
	offline, _ := cmd.Flags().GetBool("offline")
	noArchival, _ := cmd.Flags().GetBool("no-archival")
	logArchival, _ := cmd.Flags().GetBool("log-archival")

	// Normalize input
	title = validateTitle(title, "")
	excerpt = normalizeSpace(excerpt)

	// Create bookmark item
	book := model.Bookmark{
		URL:           url,
		Title:         title,
		Excerpt:       excerpt,
		CreateArchive: !noArchival,
	}

	// Set bookmark tags
	book.Tags = make([]model.Tag, len(tags))
	for i, tag := range tags {
		book.Tags[i].Name = strings.TrimSpace(tag)
	}

	// Create bookmark ID
	var err error
	book.ID, err = db.CreateNewID("bookmark")
	if err != nil {
		cError.Printf("Failed to create ID: %v\n", err)
		return
	}

	// Clean up bookmark URL
	book.URL, err = core.RemoveUTMParams(book.URL)
	if err != nil {
		cError.Printf("Failed to clean URL: %v\n", err)
		return
	}

	// If it's not offline mode, fetch data from internet.
	if !offline {
		cInfo.Println("Downloading article...")

		var isFatalErr bool
		content, contentType, err := core.DownloadBookmark(book.URL)
		if err != nil {
			cError.Printf("Failed to download: %v\n", err)
		}

		if err == nil && content != nil {
			request := core.ProcessRequest{
				DataDir:     dataDir,
				Bookmark:    book,
				Content:     content,
				ContentType: contentType,
				LogArchival: logArchival,
				KeepTitle:   title != "",
				KeepExcerpt: excerpt != "",
			}

			book, isFatalErr, err = core.ProcessBookmark(request)
			content.Close()

			if err != nil {
				cError.Printf("Failed: %v\n", err)
			}

			if isFatalErr {
				return
			}
		}
	}

	// Make sure bookmark's title not empty
	if book.Title == "" {
		book.Title = book.URL
	}

	// Save bookmark to database
	_, err = db.SaveBookmarks(book)
	if err != nil {
		cError.Printf("Failed to save bookmark: %v\n", err)
		return
	}

	// Print added bookmark
	fmt.Println()
	printBookmarks(book)
}
