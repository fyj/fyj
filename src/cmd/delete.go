package cmd

import (
	"fmt"
	"os"
	fp "path/filepath"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

func deleteCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "delete [indices]",
		Short: "删除已保存的文章书签",
		Long: `删除文章书签
删除记录后, 最后一条记录将移至已删除的索引
接受以空格为分隔的索引列表(例如, 5 6 23 4 110 45)
连字符范围(例如100-200)或者混合使用(例如, 1-3 7 9)
如果没有参数, 默认将删除所有文章书签记录`,
		Aliases: []string{"rm"},
		Run:     deleteHandler,
	}

	cmd.Flags().BoolP("yes", "y", false, "跳过确认提示, 并且删除所有文章书签(请谨慎操作)")

	return cmd
}

func deleteHandler(cmd *cobra.Command, args []string) {
	// Parse flags
	skipConfirm, _ := cmd.Flags().GetBool("yes")

	// If no arguments (i.e all bookmarks going to be deleted), confirm to user
	if len(args) == 0 && !skipConfirm {
		confirmDelete := ""
		fmt.Print("Remove ALL bookmarks? (y/N): ")
		fmt.Scanln(&confirmDelete)

		if confirmDelete != "y" {
			fmt.Println("No bookmarks deleted")
			return
		}
	}

	// Convert args to ids
	ids, err := parseStrIndices(args)
	if err != nil {
		cError.Printf("Failed to parse args: %v\n", err)
		return
	}

	// Delete bookmarks from database
	err = db.DeleteBookmarks(ids...)
	if err != nil {
		cError.Printf("Failed to delete bookmarks: %v\n", err)
		return
	}

	// Delete thumbnail image and archives from local disk
	if len(ids) == 0 {
		thumbDir := fp.Join(dataDir, "thumb")
		archiveDir := fp.Join(dataDir, "archive")
		os.RemoveAll(thumbDir)
		os.RemoveAll(archiveDir)
	} else {
		for _, id := range ids {
			strID := strconv.Itoa(id)
			imgPath := fp.Join(dataDir, "thumb", strID)
			archivePath := fp.Join(dataDir, "archive", strID)

			os.Remove(imgPath)
			os.Remove(archivePath)
		}
	}

	// Show finish message
	switch len(args) {
	case 0:
		fmt.Println("All bookmarks have been deleted")
	case 1, 2, 3, 4, 5:
		fmt.Printf("Bookmark(s) %s have been deleted\n", strings.Join(args, ", "))
	default:
		fmt.Println("Bookmark(s) have been deleted")
	}
}
