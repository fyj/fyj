package cmd

import (
	"fmt"
	"os"
	fp "path/filepath"

	gap "github.com/muesli/go-app-paths"
	"github.com/spf13/cobra"

	"gitea.com/huiyifyj/fyj/src/database"
)

var (
	db              database.DB
	dataDir         string
	developmentMode bool
)

// FyjCmd returns the root command for fyj
func FyjCmd() *cobra.Command {
	rootCmd := &cobra.Command{
		Use:   "fyj",
		Short: "fyj 是 Go 语言写的一个命令行文章书签管理工具",
		Long: `"fyj" 是一个用来管理你书签的命令行.
你也可以作为 web 服务运行在你的服务器.`,
	}

	rootCmd.PersistentPreRun = preRunRootHandler
	rootCmd.PersistentFlags().Bool("portable", false, "以便携模式运行 fyj")
	rootCmd.AddCommand(
		addCmd(),
		printCmd(),
		updateCmd(),
		deleteCmd(),
		openCmd(),
		importCmd(),
		exportCmd(),
		pocketCmd(),
		serveCmd(),
		checkCmd(),
		versionCmd(),
	)

	return rootCmd
}

func preRunRootHandler(cmd *cobra.Command, args []string) {
	// Read flag
	var err error
	portableMode, _ := cmd.Flags().GetBool("portable")

	// Get and create data dir
	dataDir, err = getDataDir(portableMode)
	if err != nil {
		cError.Printf("Failed to get data dir: %v\n", err)
		os.Exit(1)
	}

	err = os.MkdirAll(dataDir, os.ModePerm)
	if err != nil {
		cError.Printf("Failed to create data dir: %v\n", err)
		os.Exit(1)
	}

	// Open database
	db, err = openDatabase()
	if err != nil {
		cError.Printf("Failed to open database: %v\n", err)
		os.Exit(1)
	}
}

func getDataDir(portableMode bool) (string, error) {
	// If in portable mode, uses directory of executable
	if portableMode {
		exePath, err := os.Executable()
		if err != nil {
			return "", err
		}

		exeDir := fp.Dir(exePath)
		return fp.Join(exeDir, "fyj-data"), nil
	}

	if developmentMode {
		return "build/fyj-dev-data", nil
	}

	// Try to look at environment variables
	dataDir, found := os.LookupEnv("FYJ_DIR")
	if found {
		return dataDir, nil
	}

	// Try to use platform specific app path
	userScope := gap.NewScope(gap.User, "fyj")
	appDirs, err := userScope.DataDirs()
	if err == nil {
		return appDirs[0], nil
	}

	// When all fail, use current working directory
	return ".", nil
}

func openDatabase() (database.DB, error) {
	switch dbms, _ := os.LookupEnv("FYJ_DBMS"); dbms {
	case "mysql":
		return openMySQLDatabase()
	case "postgresql":
		return openPostgreSQLDatabase()
	default:
		return openSQLiteDatabase()
	}
}

// Use SQLite DataBase
func openSQLiteDatabase() (database.DB, error) {
	dbPath := fp.Join(dataDir, "fyj.db")
	return database.OpenSQLiteDatabase(dbPath)
}

// Use MySQL DataBase
func openMySQLDatabase() (database.DB, error) {
	user, _ := os.LookupEnv("FYJ_MYSQL_USER")
	password, _ := os.LookupEnv("FYJ_MYSQL_PASS")
	dbName, _ := os.LookupEnv("FYJ_MYSQL_NAME")
	dbAddress, _ := os.LookupEnv("FYJ_MYSQL_ADDRESS")

	connString := fmt.Sprintf("%s:%s@%s/%s", user, password, dbAddress, dbName)
	return database.OpenMySQLDatabase(connString)
}

// Use PostgreSQL DataBase
func openPostgreSQLDatabase() (database.DB, error) {
	host, _ := os.LookupEnv("FYJ_PG_HOST")
	port, _ := os.LookupEnv("FYJ_PG_PORT")
	user, _ := os.LookupEnv("FYJ_PG_USER")
	password, _ := os.LookupEnv("FYJ_PG_PASS")
	dbName, _ := os.LookupEnv("FYJ_PG_NAME")

	connString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbName)
	return database.OpenPGDatabase(connString)
}
