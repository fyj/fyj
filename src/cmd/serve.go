package cmd

import (
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitea.com/huiyifyj/fyj/src/webserver"
)

func serveCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "serve",
		Short: "提供用于管理文章书签的 Web 界面服务",
		Long: `以 Web 服务来运行
该服务为管理文章书签的提供网页界面服务
如果未指定 --port, 则默认情况下将使用 3000 端口号`,
		Run: serveHandler,
	}

	cmd.Flags().IntP("port", "p", 3000, "指定 Web 服务的端口号")
	cmd.Flags().StringP("address", "a", "", "指定服务器监听的地址")
	cmd.Flags().StringP("webroot", "r", "/", "指定服务器的根路径")

	return cmd
}

func serveHandler(cmd *cobra.Command, args []string) {
	// Get flags value
	port, _ := cmd.Flags().GetInt("port")
	address, _ := cmd.Flags().GetString("address")
	rootPath, _ := cmd.Flags().GetString("webroot")

	// Validate root path
	if rootPath == "" {
		rootPath = "/"
	}

	if !strings.HasPrefix(rootPath, "/") {
		rootPath = "/" + rootPath
	}

	if !strings.HasSuffix(rootPath, "/") {
		rootPath += "/"
	}

	// Start server
	serverConfig := webserver.Config{
		DB:            db,
		DataDir:       dataDir,
		ServerAddress: address,
		ServerPort:    port,
		RootPath:      rootPath,
	}

	err := webserver.ServeApp(serverConfig)
	if err != nil {
		logrus.Fatalf("Server error: %v\n", err)
	}
}
