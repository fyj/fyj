module gitea.com/huiyifyj/fyj

require (
	gitea.com/huiyifyj/readability v1.0.0
	gitea.com/huiyifyj/warc v1.0.1
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/disintegration/imaging v1.6.2
	github.com/fatih/color v1.9.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.4.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/muesli/go-app-paths v0.2.1
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v1.0.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
)

go 1.14
