// +build ignore

package main

import (
	"os"

	"gitea.com/huiyifyj/fyj/src/cmd"
)

var pathSeparator = string(os.PathSeparator)

func main() {
	cmd := cmd.FyjCmd()

	cmd.GenBashCompletionFile("build" + pathSeparator + "bash_completion.sh")
	cmd.GenZshCompletionFile("build" + pathSeparator + "zsh_completion.sh")
	cmd.GenPowerShellCompletionFile("build" + pathSeparator + "PowerShell-Completion.ps1")
}
